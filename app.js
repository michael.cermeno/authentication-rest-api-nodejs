const dotenv = require('dotenv')
const express = require('express');

dotenv.config()
require('./db')

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const usersRouter = require('./routes/users');

app.get('/', (req, res) => { res.json({ live: "ok" }) });
app.use('/users', usersRouter);



const port = process.env.PORT || 4000



app.listen(port, () => {
  console.log(`*** Servidor Corriendo en el Puerto: ${port}***`)
})
