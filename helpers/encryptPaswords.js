const bcrypts = require('bcryptjs');

const encrypt = async (password) => {
    const hash = await bcrypts.hash(password, 10)
    return hash
}
const compare = async (password, hashPasword) => {
    return await bcrypts.compare(password, hashPasword)
}
module.exports = {
    encrypt,
    compare
}