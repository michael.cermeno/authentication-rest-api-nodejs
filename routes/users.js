const express = require('express');
const router = express.Router();
const veryfyToken = require('../middlewares/verifyToken')
const { getUsers, createUser, getUserById, modifyUserId, deleteUserId, login } = require('../controllers/usersController')
const {  createSchema, modifySchema } = require('../middlewares/schemasBody');
const validator = require('../middlewares/validatorSchema')

//Obtener todos los usuarios
router.get('/',veryfyToken, getUsers)

//Crear un usuario
router.post('/create',validator(createSchema), createUser)

//obtener un usuario por su id
router.get('/user/:id',veryfyToken, getUserById)

//modificar usuario por su id
router.put('/update-user/:id',veryfyToken,validator(modifySchema), modifyUserId)

//eliminar un usuario por su id
router.delete('/deleteUser/:id',veryfyToken, deleteUserId)

//loguearse
router.post('/signin', login)

module.exports = router;
