const {  encrypt } = require('../helpers/encryptPaswords')
const express = require('express')
const usersService = require('../services/usersService')

/**
 * 
 * @param {express.Request} req 
 * @param {express.Response} res
 */
const login = async (req, res) => {
    try {
        const { username, password } = req.body
        
        
        const token = await usersService.login(username, password)
        res.json({ mesagge:" Autenticacion exitosa" ,token })
    } catch (error) {
        res.status(500).json({
            message: error.message
        }).end()
    }
}

/**
 * 
 * @param {express.Request} req 
 * @param {express.Response} res
 */
const getUsers = async (req, res) => {
    try {
        
        const allUsers = await usersService.getUsers()
        res.json(allUsers)
    } catch (error) {
        res.status(500).json({
            message: error.message
        }).end()
    }

}

/**
 * 
 * @param {express.Request} req 
 * @param {express.Response} res
 */
const createUser = async (req, res) => {
    try {
        const body = req.body
        const pass = body.password
        const hashPass =  await encrypt(pass)
        req.body.password = hashPass
        const result = await usersService.createUser(body)
        res.json(result)
    } catch (error) {
        console.log(error)
        res.status(500).json({
            message: error.message
        }).end()
    }
}
/**
 * 
 * @param {express.Request} req 
 * @param {express.Response} res
 */
const getUserById = async (req, res) => {
    try {
        const id = req.params.id
        const user = await usersService.getUserById(id)
        res.json(user)
    } catch (error) {
        res.status(400).json({
            message: "Usuario no Encontrado"
        })
    }
}
/**
 * 
 * @param {express.Request} req 
 * @param {express.Response} res
 */
const modifyUserId = async (req, res) => {
    try {
        const { id } = req.params
        const user = await usersService.modifyUserId(id, req.body)
        console.log(user)
        res.send(user)
    } catch (error) {
        console.log(error)
        res.status(400).json({
            message: "Usuario no Actualizado"
        })
    }
}
/**
 * 
 * @param {express.Request} req 
 * @param {express.Response} res
 */
const deleteUserId = async (req, res) => {

    try {
        const id = req.params.id
        const user = await usersService.deleteUserId(id)
        if(!user){
            res.status(404).send('User not Found')
        }
        res.json({
            message: ` ${user.username} Deleted`
        })
    } catch (error) {
        res.status(400).json({
            "Message" : "Usuario no ha sido Eliminado"
        })
    }

}

module.exports = {
    getUsers,
    createUser,
    getUserById,
    modifyUserId,
    deleteUserId,
    login
}