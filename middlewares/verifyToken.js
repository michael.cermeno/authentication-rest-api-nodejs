const express = require('express')
const jwt = require('jsonwebtoken')
/**
 * 
 * @param {express.Request} req 
 * @param {express.Response} res
 */

const veryfyToken = (req, res, next) => {
    try {
        if (!req.headers.authorization) {
            return res.sendStatus(401).send('Unauthorized request')
        }
        const token = req.headers.authorization.split(' ')[1]
        if (token === null) {
            return res.sendStatus(401).send('Unauthorized request')
        }
        const payload = jwt.verify(token, process.env.SECRET_KEY)
        next()
    } catch (error) {
        res.status(401).send('Invalid token')
    }
}
module.exports = veryfyToken;