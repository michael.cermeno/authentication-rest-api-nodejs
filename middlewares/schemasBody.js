const joi = require('joi')

//creacion de Usuario
const createSchema = joi.object({
    name: joi.string().min(3).max(20).required(),
    lastname: joi.string().min(3).max(20).required(),
    email: joi.string().email().required(),
    username: joi.string().alphanum().min(3).max(20).required(),
    password: joi.string().alphanum().min(6).required(),
    photo: joi.string().optional(),
    birthday: joi.date().greater('1950-1-1').less('2004-12-31') ,
    documentType: joi.string().required(),
    documentNumber: joi.number().required(),
    cellphone: joi.number().required()

})


// modificacion de usuario
const modifySchema = joi.object({
    name: joi.string().alphanum().min(3).max(20).optional(),
    lastname: joi.string().alphanum().min(3).max(20).optional(),
    email: joi.string().email().optional(),
    photo: joi.string().optional(),
    cellphone: joi.number().min(10).optional(),
})

module.exports = {
    createSchema,
    modifySchema,
}