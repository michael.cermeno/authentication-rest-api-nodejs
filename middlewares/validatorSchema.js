
module.exports = (data) => {
    return  async ( req, res, next ) => {
        try {
            await data.validateAsync(req.body)
            next()
        } catch (error) {
            res.send(error.message);
        }
    }
}