const jwt = require('jsonwebtoken')
const UserModel = require('../model/UserModel')
const UserLogin = require('../model/loginModel')
require('dotenv')
const { compare } = require('../helpers/encryptPaswords')
const KEY = process.env.SECRET_KEY
const getUsers = async () => {
    const users = await UserModel.find({})
    return users
}

//////////////////Crear usuario

const createUser = async (userParams) => {
    const user = new UserModel({ ...userParams, active: true })
    const username = userParams.username
    const documentNumber = userParams.documentNumber
    const searchDocument = await UserModel.findOne({ documentNumber })
    const searchUsername = await UserModel.findOne({ username })
    if (searchUsername) {
        throw new Error('Username ya estan siendo usado')
    }
    if (searchDocument) {
        throw new Error(' Este documento ya estan siendo usado')
    }
    const serchLogin = await UserLogin.findOne({username})
    if(!serchLogin){
        const userLogin = await UserLogin({username, token:''})
        await userLogin.save()
    }
    const result = await user.save()
    return result;
}

////////// Buscar usuario por id

const getUserById = async (id) => {
    const user = await UserModel.findById(id)
    return user
}

//////////////Modificar usuario por id

const modifyUserId = async (id, userProps) => {
    const user = await UserModel.findByIdAndUpdate(id)
    for (const field in userProps) {
        if (userProps[field]) {
            user[field] = userProps[field]
        }
    }

    await  user.save()
    return user
}

/////////////Eliminar usuario por id

const deleteUserId = async (id) => {
    const userId = await UserModel.findByIdAndDelete(id)
    return userId
}

/////////////loguearse

const login = async (username, password) => {
    const user = await UserModel.findOne({ username })
    if (!user) {
        throw new Error('User_Not_Found')
    }
    const checkPassword = await compare(password, user.password)
    if (!checkPassword) {
        throw new Error('Invalid Password')
    }
    const token = jwt.sign({ user }, KEY, { expiresIn: 1800 })
    const userLogin = await UserLogin.findOneAndUpdate({username,token})
    
    await userLogin.save()
    return token

}
module.exports = {
    getUsers,
    createUser,
    getUserById,
    modifyUserId,
    deleteUserId,
    login
}  