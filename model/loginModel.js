const mongoose = require('mongoose');   
const { Schema, model  } = mongoose

const loginSchema = new Schema({
    username : { type: String, required: true},
    token : String
},{
    timestamps: true
})
const loginModel = model('login',loginSchema)
module.exports = loginModel;
