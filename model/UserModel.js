const mongoose = require('mongoose')
const { Schema , model } = mongoose
const userSchema = new Schema({
    name: { type: String, required: true },
    lastname: { type: String, required: true },
    email: { type: String, required: true },
    username: String,
    password: String,
    photo: String,
    birthday: { type: Date, required: true },
    session_count: Number,
    active: { type: Boolean, required: true },
    documentType: { type: String,required: true},
    documentNumber: { type: Number, required: true },
    cellphone: { type: Number, required: true }
},{
    timestamps: true
})
//userSchema.methods.encryptPassword = (password)
const UserModel = model('User', userSchema)

module.exports = UserModel