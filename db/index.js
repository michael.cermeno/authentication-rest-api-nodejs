const urlConnection = process.env.DB_URL
const mongoose = require('mongoose');
mongoose.connect(urlConnection);


const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
    console.log("*** Base de Datos Conectada***");
});
